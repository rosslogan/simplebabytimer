import React, { Component } from 'react'
import { Provider } from 'react-redux'

import store from '@application/store'
import Navigation from '@application/routes'

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Navigation />
      </Provider>
    );
  }
}

export default App
