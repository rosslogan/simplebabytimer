import {
  SET_FEED_TYPE,
} from '@config/actionTypes'

export const initialState = {
  feedType: 'breast',
  language: 'en',
}

const reducer = (state = initialState, action = {}) => {
  const { type, payload } = action

  switch (type) {
    case SET_FEED_TYPE: {
      return {
        ...state,
        feedType: payload.feedType,
      }
    }

    default:
      return state

  }
}

export default reducer
