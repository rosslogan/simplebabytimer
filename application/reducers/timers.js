import {
  SET_TIMER,
} from '@config/actionTypes'

export const initialState = {
  timer: null,
}

const reducer = (state = initialState, action = {}) => {
  const { type, payload } = action

  switch (type) {
    case SET_TIMER: {
      return {
        ...state,
        timer: payload.timer,
      }
    }

    default:
      return state

  }
}

export default reducer
