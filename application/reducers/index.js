import { persistCombineReducers } from 'redux-persist'
import storage from 'redux-persist/lib/storage'

import timers from '@reducers/timers'
import settings from '@reducers/settings'

const config = {
  key: 'primary',
  storage
}

export default persistCombineReducers(config, {
  timers,
  settings,
})