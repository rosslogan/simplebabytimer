import { applyMiddleware, compose, createStore } from 'redux'
import reducers from '@reducers'
import { createLogger } from 'redux-logger'
import { persistStore } from 'redux-persist'

const logger = createLogger()

const store = createStore(
  reducers,
  compose(
    applyMiddleware(logger),
  )
)

persistStore(
  store,
  null,
  () => {
    store.getState()
  }
)

export default store