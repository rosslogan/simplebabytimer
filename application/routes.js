import React from 'react'
import { createAppContainer, createBottomTabNavigator } from 'react-navigation'
import Icon from 'react-native-vector-icons/SimpleLineIcons'

import TimersScreen from '@screens/timers/Timers'
//import ReportsScreen from '@screens/reports/Reports'
import SettingsScreen from '@screens/settings/Settings'
import { theme } from './styles/theme'

const RootNav = createBottomTabNavigator(
  {
    Timers: TimersScreen,
    // Reports: ReportsScreen,
    Settings: SettingsScreen,
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ tintColor }) => {
        const { routeName } = navigation.state;
        let iconName;
        if (routeName === 'Timers') {
          iconName = 'clock'
        // } else if (routeName === 'Reports') {
        //   iconName = 'notebook'
        } else if (routeName === 'Settings') {
          iconName = 'settings'
        }

        return <Icon name={iconName} size={18} color={tintColor} />;
      },
    }),
    tabBarOptions: {
      activeTintColor: theme.activeTabColor,
      inactiveTintColor: theme.tabColor,
    },
  }
);

export default createAppContainer(RootNav);