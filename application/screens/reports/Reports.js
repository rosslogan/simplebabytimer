import React, { Component } from 'react'
import { StyleSheet, Text, View } from 'react-native'

class Reports extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.text}>Reports</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  text: {
    fontSize: 20,
    textAlign: 'center',
  },
})


export default Reports
