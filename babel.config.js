module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    [
      'module-resolver',
      {
        cwd: 'babelrc',
        alias: {
          '@application': './application',
          '@actions': './application/actions',
          '@components': './application/components',
          '@config': './application/config',
          '@constants': './application/constants',
          '@images': './application/images',
          '@reducers': './application/reducers',
          '@screens': './application/screens',
          '@styles': './application/styles',
        },
      },
    ],
  ],
}